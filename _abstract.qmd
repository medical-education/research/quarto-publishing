This project aims to explore the application of Quarto, an open-source publishing system, in the field of medical education. Our focus is on promoting open, reproducible research through cloud-based platforms that enable device-independent access.

**Background:**\
[Quarto](https://quarto.org) is an open-source scientific and technical publishing system which uses Pandoc markdown for reproducible and dynamic content.\
The aim of this project is to identify ways in which Quarto can be used in the field of medical education to publish scientifically.

**Methods:**\
Our group is committed to the values of open and reproducible research. With this project we want to promote these values in the field of medical education. Our goal is open access and open materials.

To achieve these goals, this project is hosted on several platforms that support different aspects of Open Science. What these platforms have in common is that they are all cloud solutions to enable device-independent access.

**Results:**\
What we have achieved in this project so far:

-   We host all materials on GitLab for versioning and use GitLab Pages to host a static [project-website](https://medical-education.pages.ub.uni-bielefeld.de/research/quarto-publishing/) directly from your GitLab repository.

-   We render manuscripts via [Quarto](https://quarto.org) in the [Posit Cloud](https://posit.co/products/cloud/cloud/) to publish reproducible, production quality outputs in a wide variety of formats.

-   We share documents via [sciebo](https://hochschulcloud.nrw) to enable cooperation with non-data-scientists.

-   We use the [Open Science Framework](https://osf.io) to facilitate science research with special tools for open project collaboration.

Future intended topics are:

-   Directory structure of a project {{< fa folder >}}

-   Aside-References, like [@gorlich2021using].

-   Authorship

-   Templates

-   Identifier

-   Citations

-   ...

**Discussion:**

... in progress ... `r emo::ji("smile")`
