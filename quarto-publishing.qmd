---
title: "Scientific publishing with Quarto in Medical Education"
author: Hendrik Friederichs
metadata-files: [_authordata.yml, _citationdata.yml]
filters:
  - authors-block
  - abstract-section
engine: knitr
---

# Abstract

{{< include _abstract.qmd >}}

# References

