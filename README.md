# Scientific publishing with Quarto in Medical Education

Quarto is an open-source scientific and technical publishing system which uses Pandoc markdown for reproducible and dynamic content.  

The aim of this project is to identify ways in which Quarto can be used in the field of medical education to publish scientifically.
